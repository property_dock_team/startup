<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarlystagestartupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earlystagestartups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('applicantName');
            $table->mediumText('IDNumber');
            $table->mediumText('accupation');
            $table->mediumText('emailAddress');
            $table->mediumText('residence');
            $table->mediumText('phoneNumber');
            $table->longText('partners');
            $table->mediumText('industry');
            $table->mediumText('comodityOffered');
            $table->longText('ProductDescription');
            $table->mediumText('competitiveadvantage');
            $table->mediumText('supplyChainLevel');
            $table->mediumText('productReusability');
            $table->mediumText('amount');
            $table->longText('investmentInstrument');
            $table->mediumText('projectCost');
            $table->mediumText('loanPurpose');
            $table->mediumText('personalContribution');
            $table->mediumText('liability');
            $table->mediumText('mainbusinessCost');
            

            $table->mediumText('targetMarket');
            $table->mediumText('incomeGroup');
            $table->mediumText('industryGiants');
            $table->mediumText('deliveryMode');
            $table->mediumText('marketingMode');
            $table->longText('resellability');
            $table->mediumText('businessScalability'); 
            
            $table->timestamps('created_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earlystagestartups');
    }
}
