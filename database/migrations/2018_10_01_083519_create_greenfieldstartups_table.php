<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGreenfieldstartupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('greenfieldstartups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('companyName');
            $table->mediumText('phoneContact');
            $table->mediumText('EmailAddress');
            $table->mediumText('CEOName');
            $table->mediumText('location');
            $table->mediumText('businessType');
            $table->mediumText('KRAPIN');
            $table->mediumText('ProductOrService');
            $table->mediumText('CommodityType');
            $table->longText('Description');
            $table->mediumText('CompetitiveAdvanage');

            $table->mediumText('supplyChain');
            $table->mediumText('reusability');
            $table->mediumText('amount');
            $table->mediumText('investmentInstrument');
            $table->mediumText('projectCost');
            $table->mediumText('Purpose');
            $table->mediumText('personalContribution');
            $table->mediumText('Liability');
            $table->mediumText('mainbusinessCost');
            $table->mediumText('targetmarket');
            $table->mediumText('incomeGroup');
            $table->mediumText('industrygiants');
            $table->mediumText('deliveryMode');
            $table->mediumText('marketingMode');
            $table->mediumText('productResellability');
            $table->mediumText('businessScalability');
            $table->mediumText('fileBusinessPlan');
            
            $table->timestamps('created_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('greenfieldstartups');
    }
}
