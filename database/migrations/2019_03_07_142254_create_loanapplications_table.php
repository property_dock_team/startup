<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loanapplications', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('Applicantname');
            $table->mediumText('IDNumber');
            $table->mediumText('Dateofbirth');
            $table->mediumText('Emailaddress');
            $table->mediumText('residenttown');
            $table->mediumText('phonenumber');
            $table->mediumText('maritalStatus');
            $table->mediumText('gender');
            $table->mediumText('IncomeType');
            $table->mediumText('Employername');
            $table->mediumText('businessaddress');
            $table->mediumText('jobdescription');
            $table->mediumText('incomerange');
            $table->mediumText('educationlevel');
            $table->mediumText('seedamount');
            $table->mediumText('projectcost');
            $table->mediumText('repaymentperiod');
            $table->mediumText('purpose');
            $table->mediumText('personalContribution');
            $table->mediumText('currentdebtstatus');
           
            $table->timestamps('created_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanapplications');
    }
}
