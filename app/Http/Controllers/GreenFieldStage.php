<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\greenfieldstartup;
use DB;


class GreenFieldStage extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if($request->hasFile('fileBusinessPlan')){
            $fileNameWithEx=$request->file('fileBusinessPlan')->getClientOriginalName();
            $fileName=pathinfo($fileNameWithEx,PATHINFO_FILENAME);
            $extension=$request->file('fileBusinessPlan')->getClientOriginalExtension();
            $fileNameToStore=$fileName.'_'.time().'.'.$extension;
            $path=$request->file('fileBusinessPlan')->storeAs('public/BusinessPlans',$fileNameToStore);

        }else{


        }
    
        $postData= new greenfieldstartup;

        $postData->companyName=$request->input('companyName');
        $postData->phoneContact=$request->input('phoneContact');
        $postData->EmailAddress=$request->input('EmailAddress');
        $postData->CEOName=$request->input('CEOName');
        $postData->location=$request->input('location');
        $postData->businessType=$request->input('businessType');
        $postData->KRAPIN=$request->input('KRAPIN');
        $postData->ProductOrService=$request->input('ProductOrService');
        $postData->CommodityType=$request->input('CommodityType');
        $postData->Description=$request->input('Description');
        $postData->CompetitiveAdvanage=$request->input('CompetitiveAdvanage');
        $postData->supplyChain=$request->input('supplyChain');
        $postData->reusability=$request->input('reusability');
        $postData->amount=$request->input('amount');
        $postData->investmentInstrument=$request->input('investmentInstrument');
        $postData->projectCost=$request->input('projectCost');
        $postData->Purpose=$request->input('Purpose');
        $postData->personalContribution=$request->input('personalContribution');
        $postData->Liability=$request->input('Liability');
        $postData->mainbusinessCost=$request->input('mainbusinessCost');
        $postData->targetmarket=$request->input('targetmarket');
        $postData->incomeGroup=$request->input('incomeGroup');
        $postData->industrygiants=$request->input('industrygiants');
        $postData->deliveryMode=$request->input('deliveryMode');
        $postData->marketingMode=$request->input('marketingMode');
        $postData->productResellability=$request->input('productResellability');
        $postData->businessScalability=$request->input('businessScalability');
        $postData->fileBusinessPlan=$fileNameToStore;
        $postData->save();

        return view('GreenFieldstagestartups.store')->with('success','Application Filled Successfilly');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
