<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\earlystagestartup;
use DB;

class EarlyStage extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        
        $postData=new earlystagestartup;

        $postData->applicantName=$request->input('applicantName');
        $postData->IDNumber=$request->input('IDNumber');
        $postData->accupation=$request->input('accupation');
        $postData->emailAddress=$request->input('emailAddress');
        $postData->residence=$request->input('residence');
        $postData->phoneNumber=$request->input('phoneNumber');
        $postData->partners=$request->input('partners');
        $postData->industry=$request->input('industry');
        $postData->comodityOffered=$request->input('comodityOffered');
        $postData->ProductDescription=$request->input('ProductDescription');
        $postData->competitiveadvantage=$request->input('competitiveadvantage');
        $postData->supplyChainLevel=$request->input('supplyChainLevel');
        $postData->productReusability=$request->input('productReusability');
        $postData->amount=$request->input('amount');
        $postData->investmentInstrument=$request->input('investmentInstrument');
        $postData->projectCost=$request->input('projectCost');
        $postData->loanPurpose=$request->input('loanPurpose');
        $postData->personalContribution=$request->input('personalContribution');
        $postData->liability=$request->input('liability');
        $postData->mainbusinessCost=$request->input('mainbusinessCost');
        $postData->targetMarket=$request->input('targetMarket');
        $postData->incomeGroup=$request->input('incomeGroup');
        $postData->industryGiants=$request->input('industryGiants');
        $postData->deliveryMode=$request->input('deliveryMode');
        $postData->marketingMode=$request->input('marketingMode');
        $postData->resellability=$request->input('resellability');
        $postData->businessScalability=$request->input('businessScalability');
        $postData->save();

        return view('Earlystagestartups.store')->with('success','Application Filled Successfilly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
