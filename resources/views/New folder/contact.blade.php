@extends('layouts.appMain')
@section('content')
    <body>
        
        <!-- Carousel
    ================================================== -->
        <!-- /.carousel -->
        <!-- Marketing messaging and featurettes
    ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->
        <div class="container">
            <div class="container">
                <div class="row">
                    <h2 class="headingh">Contact Us</h2>
                    <div class="col-md-4">
                        
                        <p class="">Email Us On :  info@startup.co.ke <br>
                       Call  Us On : +254715576211,+254729379752<br>
                       Locate Us : Rumathi House 1st floor Kiambu</p> 
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-md-4">
                    <form role="form" class=""> 
                        <div class="form-group"> 
                            <h3>Email Us On:<br></h3>
                            <label class="control-label" for="exampleInputEmail1">Email address</label>                             
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> 
                        </div>
                        <div class="form-group"> 
                            <label class="control-label" for="formInput121">Name:
                                <br>
                            </label>
                            <input type="text" class="form-control" id="formInput121" placeholder="Your Name">
                        </div>
                        <div class="form-group"> 
                            <label class="control-label" for="formInput113">Phone Number:</label>
                            <input type="text" class="form-control" id="formInput113" placeholder="Phone Number">
                        </div>
                        <div class="form-group"> 
                            <label class="control-label" for="formInput77">Your Message:</label>                             
                            <textarea class="form-control" rows="3" id="formInput77"></textarea>
                        </div>                         
                        <button type="submit" class="btn">Submit</button>                         
                    </form>                     
                </div>
                <div class="col-md-4">
                    <div class="row featurette">
                        <div class="row"></div>
                        <map>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1994.494951254367!2d36.82423495595486!3d-1.1675978991483356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f3c61fca56f8d%3A0xdd2d30b81e51bd6!2sRumathi+House%2C+Kiambu%2C+Kenya!5e0!3m2!1sen!2s!4v1504873475765" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </map>
                    </div>                     
                </div>
            </div>
        </div>
        
        <!-- /.container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script src="assets/js/holder.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
@endsection
