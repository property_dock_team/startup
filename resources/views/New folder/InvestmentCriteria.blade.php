
@extends('layouts.app')
@section('content')


<body>



	<div class="row">	
        <div class="container">
    	
        	<h1>Investment Criteria</h1><br>

			<p>We seek profitable businesses with the following features:</p>
					 
             <ul class="checklist">
                <li>The ability to generate strong, sustainable cash flows and deliver an Internal Rate of Return of at least 30%.</li> 
                <li>An investment requirement of between Ksh.50,000 and Ksh. 1.5M. This may be structured as an equity investment, or as a participating/convertible loan. The entrepreneur must have evidence of what he or she will contribute to the venture in cash or assets.</li>
                <li>A management team, with a relevant track record, exists or can be mobilized (as we cannot provide management expertise). This team should be open to working with active and involved hands-on investors.</li>
                <li>An initial offering price is in place. This will help us quickly decide whether the opportunity is attractive.</li>
                <li>Clear opportunities for  investor to exit .</li>
            </ul>

            <br>
                
            <table class="table">
              <tbody>
                  <tr>
                      <th colspan="2">Funding Criteria</th>
                  </tr>
                  <tr>
                      <td width="30%"><strong>Geographic Focus</strong></td>
                      <td width="70%">Startup ventures will invest only in companies incorporated in, or enterprises considering expansion into, Kenya.</td>
                  </tr>
                  <tr class="alt">
                      <td><strong>Company Stage</strong></td>
                      <td>Startup ventures will invest in startups and expansion-stage companies.</td>
                  </tr>
                  <tr>
                      <td><strong>Industry Focus</strong></td>
                      <td>Startup ventures has a broad industry focus. We generally prefer agribusinesses, education, healthcare, and general industry,Technology, Entertainment, Transport and general industry
                  </tr>
                  <tr class="alt">
                      <td><strong>Investment Size</strong></td>
                      <td>Startup ventures will invest Ksh.50,000 to Ksh. 1.5M in each portfolio company. The investment company will generally invest in two or more rounds based on the accomplishment of milestones by the portfolio company.</td>
                  </tr>
                  <tr>
                      <td><strong>Type of Security</strong></td>
                      <td>Startup ventures  generally will make equity investments in preferred stock and loan investments in convertible or participating loans.</td>
                  </tr>
                  <tr class="alt">
                      <td><strong>Investment Term</strong></td>
                      <td>The anticipated time for holding a typical investment is three to five years.</td>
                  </tr>
                  <tr>
                      <td><strong>Likely Exit Scenarios</strong></td>
                      <td>The most likely exit scenarios are trade sales or buyouts by the entrepreneur. However, other exit scenarios are possible.</td>
                  </tr>
                  <tr class="alt">
                      <td><strong>Co-investors</strong></td>
                      <td>Startup ventures  will be a sole investor in many cases given that it takes higher risk than other investors are typically able to. Occasionally, in cases where the required investment exceeds our maximum investment size, Startup may invest as part of an investment syndicate or co-invest in a round led by another institutional investor. When appropriate, Startup ventures  will co-invest with  other financial institutions.</td>
                  </tr>
              </tbody>
            </table>

            <br><br>
			<p>Entrepreneurs with businesses or business plans that meet the above criteria in Kenya are invited to contact us or to submit their invitation to invest to us for our consideration.</p> 
            <a class="btn btn-default" href="investmentmodel" role="button">APPLY HERE</a><code>Click here to contact us and submit your business plan.</code></a>
            
         <br><br>
	    </div>
 </div>		<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	
   

</body>
@endsection