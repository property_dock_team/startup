@extends('layouts.appMain')
@section('content')
    <body>
        
        <div class="container">
          
            <!-- Jumbotron -->
            <div class="jumbotron" style="background-image: url('Images/crowdfunding/funding.png');  background-size:100% 100%;height: 300px;">
               
                <br>
                <p class="lead"><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a><br></p>
            </div>
            <h3>Types of CrowdFunding</h3>
            <div class="row marketing">
                <div class="col-lg-4">
                    <h3>Donation Based Crowdfunding</h3>
                    <img class="img-circle" src="Images/crowdfunding/Donate.png" alt="Generic placeholder image" width="190" height="190">
                
                    <p>Like the name suggests, in a lot of donation-based crowdfunding, there isn’t always a reward beyond the gratitude of the project creator or beneficiary (and possibly a tax deduction). Donation-based crowdfunding is typically used to raise money for a non-profit or a cause, like drilling a well or building a school  or for a personal campaign like an individual’s treatment or medical bills.</p>
                   </div>
                <div class="col-lg-4">
                <h3>Eqity Based Crowdfunding</h3>
                <img class="img-circle" src="Images/crowdfunding/equity-crowdfunding.png" alt="Generic placeholder image" width="190" height="190">
                
                    <p>In equity-crowdfunding, investors give larger amounts of money . When investors give the money, they don’t get a reward, but instead, a small piece of equity in the company itself.

As a result, equity crowdfunding is typically used to raise money to fund the launch or growth of a company, not just initiate a creative project or cause. Often, these companies go on to raise money from angel investors or venture capitalists.</p>
                    
                </div>
                <div class="col-lg-4">
                <h3>Debt Based Crowdfunding</h3>

                <img class="img-circle" src="Images/crowdfunding/images.jpg" alt="Generic placeholder image" width="190" height="190">
                
                    <p>In debt-crowdfunding, it’s not “backers” or “donors” who give money, but Private lenders (sometimes these lenders are called investors).

Unlike other forms of crowdfunding, it’s NOT an exchange for a reward or equity. The investors don’t get a reward and they don’t get a piece of equity in the company, but instead they make a loan with the expectation to get paid back the principal plus interest.

So it’s a lot like loan from the bank, but instead of borrowing one larger amount of money from one bank, you borrow smaller amounts of money from multiple people.</p>
                 
                </div>
            </div>


        
        </div>         
        <!-- /container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
@endsection
