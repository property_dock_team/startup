@extends('layouts.app')
@section('content')
<body>
		
	<div class="row">

		<div class="container">
					<h1>Investment Instruments</h1>

					<p class="justified"></p><blockquote class="blockquote-left">Startup ventures is flexible in the investment instruments it can use.</blockquote> Depending on the needs of the different businesses/ entrepreneurs, instruments that Startup capital can deploy include: equity capital, royalty-participating loans, convertible loans and other mezzanine instruments. <p></p> 
					<p class="justified">Indicatively, in the table below we show where different instruments are most likely to be deployed. Keep in mind that this will vary based on the entrepreneur and the business characteristics.</p> 

                    <table class="table">
                      <tbody>
                          <tr>
                              <th>Business Stage</th>
                              <th>Asset Base</th>
                              <th>Instrument</th>
                          </tr>
                          <tr>
                              <td><strong>Early/Startup</strong></td>
                              <td>Thin</td>
                              <td>Equity</td>
                          </tr>
                          <tr class="alt">
                              <td><strong>Expansion</strong></td>
                              <td>Medium</td>
                              <td>Equity; Convertible Loan; Participating Loan</td>
                          </tr>
                          <tr>
                              <td><strong>Greenfield</strong></td>
                              <td>Large</td>
                              <td>Secured Loan; Equity</td>
                          </tr>
                      </tbody>
                    </table>
                    
					<p class="justified">Startup ventures can also identify other financial institutions to co-finance the projects.</p> 
                    <div class="row">
                    <p><h3>Choose  Application below based on your company's growth stage</h3></p>
                    </div>

                    <div class="row">
                    <a class="btn btn-primary" href="/earlystagestartup" role="button">Early Stage Startup</a>
                    <a class="btn btn-primary" href="/expansionstagestartup" role="button">Expansion Stage Startup</a>
                    <a class="btn btn-primary" href="/greenfieldstartup" role="button">Greenfield Stage Startup</a>
                    </div>
				<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		
			
            </div>
	</div>
	

</body>
@endsection