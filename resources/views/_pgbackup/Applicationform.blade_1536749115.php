<div class="masthead">
    <h3 class="text-muted">Start up</h3>
    <nav class="navbar navbar-inverse">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="/">Home</a>
            </li>
            <li>
                <a href="Investor">Investors<br></a>
            </li>
            <li>
                <a href="Entrepreneur">Enterpreneur<br></a>
            </li>
            <li>
                <a href="CrowdFunding">Crowd Funding</a>
            </li>
            <li>
                <a href="loans">Loans and Credit</a>
            </li>
            <li>
                <a href="contact">Contact</a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="login">Log in</a>
            </li>
            <li>
                <a href="register">Sign up</a>
            </li>
        </ul>
    </nav>
</div>
<form>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group">
        <label for="exampleInputFile">File input</label>
        <input type="file" id="exampleInputFile">
        <p class="help-block">Example block-level help text here.</p>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox"> Check me out
        </label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
