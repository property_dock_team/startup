<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/img-20180813-wa0009-122x122.jpg" type="image/x-icon">
  <meta name="description" content="Web Page Creator Description">
  <title>Crowd Funding</title>
  <!-- <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css"> -->


  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  
</head>
<body>
  {{--  <section class="menu cid-r6WJNeA9ds" once="menu" id="menu2-11">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="#">website templates</a></section><section class="header3 cid-r6WyQZLRVw mbr-parallax-background" id="header3-o">

    

    

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-figure" style="width: 60%;">
                <img src="{{  asset('css/assets/images/crowdfunding-892x539.jpg')}}" alt="" title="">
            </div>

            <div class="media-content">
                
                
                <div class="mbr-section-text mbr-white pb-3 ">
                    
                </div>
              
        </div>
      
    </div>
    <div class="mbr-section-btn"><a class="btn btn-md btn-primary display-4" href="#features3-q">LEARN MORE</a>
        <a class="btn btn-md btn-primary display-4" href="#">Getting started</a></div>
</div>
</section>

<section class="features3 cid-r6Wz0lqIGU" id="features3-q">

    

    
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/donate-259x202.png')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Donation Based Crowdfunding</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Like the name suggests, in a lot of donation-based crowdfunding, there isn’t always a reward beyond the gratitude of the project creator or beneficiary (and possibly a tax deduction). Donation-based crowdfunding is typically used to raise money for a non-profit or a cause, like drilling a well or building a school or for a personal campaign like an individual’s treatment or medical bills.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/equity-crowdfunding-676x351.png')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Eqity Based Crowdfunding</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            In equity-crowdfunding, investors give larger amounts of money . When investors give the money, they don’t get a reward, but instead, a small piece of equity in the company itself. As a result, equity crowdfunding is typically used to raise money to fund the launch or growth of a company, not just initiate a creative project or cause. Often, these companies go on to raise money from angel investors or venture capitalists.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/images-275x183.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Debt Based Crowdfunding</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            In debt-crowdfunding, it’s not “backers” or “donors” who give money, but Private lenders (sometimes these lenders are called investors). Unlike other forms of crowdfunding, it’s NOT an exchange for a reward or equity. The investors don’t get a reward and they don’t get a piece of equity in the company, but instead they make a loan with the expectation to get paid back the principal plus interest. So it’s a lot like loan from the bank, but instead of borrowing one larger amount of money from one bank, you borrow smaller amounts of money from multiple people.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            
        </div>
    </div>
</section>





  <script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
  <script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
  <script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
  <script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
  <script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
  <script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  
  
</body>
@endsection