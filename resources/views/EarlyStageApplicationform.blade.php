@extends('layouts.appMain')
@section('content')
<section class="mbr-section info2 cid-r7c4jiDTtS"><a href="">free website creation software</a></section><section class="mbr-section info2 cid-r7c4jiDTtS" id="header12-6">

<div class="row" style="margin-left: 20px">
    {{--  <div class="col-md-6 ">
            {!! Form::open(['action'=>'PostController@store', 'method'=>'POST'])!!}
                <div class="form-group">
                    <h3>Company Name</h3>
                    <label for="exampleInputEmail1"> Please enter the name of your company:</label>
                    <input type="email" class="form-control input" id="exampleInputEmail1" placeholder="">
                </div>
                <div class="form-group">
                    <h5>Management Team Information</h5>
                    <label for="exampleInputEmail1">Founders, other C- level Executives and Employees by function.</label>
                    <textarea class="form-control" rows="3"></textarea>
                    <label for="exampleInputEmail1">What is the length of time the management team has worked together? And in what capacity?</label>
                    <textarea class="form-control" rows="2"></textarea>             
                    <label for="exampleInputEmail1">List of Board of Directors and Advisors</label>
                    <textarea class="form-control" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <h5>Company Summary </h5>
                    <label for="exampleInputEmail1"> Please enter your short pitch here:</label>
                    <textarea class="form-control" rows="5"></textarea>
                </div>
               
     </div>  --}}
     <div class="col-md-5">
        {!! Form::open(['action'=>'EarlyStage@store', 'method'=>'POST'])!!}
        <div class="form-group">
            <h3> PERSONAL INFORMATION</h3>
          <label for="exampleInputEmail1">Applicant Name </label>
          <input type="text" required="" class="form-control" name="applicantName" placeholder="">
        </div>
      
       <div class="form-group">
            
          <label for="exampleInputEmail1">National ID/Passport No.</label>
          <input type="text" required=""  class="form-control" name="IDNumber" placeholder="">
        </div>
      
        <div class="form-group">
            
       
          <label for="exampleInputEmail1">Occupation</label>
              <select class="form-control" name="accupation">
              <option>Employed</option>
              <option>Self Employed</option>
              <option>Student</option>
              <option>Business Owner</option>
            </select>
      
        </div>
      
         <div class="form-group">
            
       
          <label for="exampleInputEmail1">Email Address</label>
          <input type="email" required=""  class="form-control" name="emailAddress" placeholder="">
      
        </div>
      
        <div class="form-group">
            
       
          <label for="exampleInputEmail1">Place Of Residence</label>
          <input type="text" required="" class="form-control" name="residence" placeholder="">
      
        </div>
      
         <div class="form-group">
            
       
          <label for="exampleInputEmail1">Telephone No</label>
          <input type="text" required="" class="form-control" name="phoneNumber" placeholder="">
      
        </div>
      
          <div class="form-group">
            
                <label for="exampleInputEmail1"> List All your partners if Any </label>
                <textarea class="form-control" required=""  rows="2" name="partners"></textarea>
                               
            </div>
      
    </div>
       <div class="col-md-6 ">
        
            
        <div class="form-group">

                <h3>Product or Service Offered</h3>
            <label for="sel1">Industry:</label>
            <select class="form-control" name="industry">
              <option>Software Tech</option>
              <option>Agri-Business</option>
              <option>Education</option>
              <option>Entertainment</option>

              <option>Bars & Restaurants</option>
              <option>Designer & Fashion Ware</option>
              <option>Building & Construction</option>
              <option>Environment</option>
              <option>Health Care clinics</option>
              <option>Liquor, Wine & Beer</option>
              <option>Hotel & Catering</option>
              <option>Music Production</option>
              <option>Pharmaceuticals & Health Products</option>
              <option>Poultry & Eggs</option>
              <option>Printing & Publishing</option>
              <option>Real Estate</option>

              <option>Retail Sales</option>
              <option>Private Sequrity</option>
              <option>Transport</option>
              <option>Sports</option>
              <option>Textiles </option>
              <option>Trucking & Logistics</option>
              <option>Vegetables & Fruits Farming</option>
              <option>Waste Management</option>
              <option>Dairy Farming</option>
              <option>Poultry & Eggs Farming</option>
              <option>Travel $ Tourism</option>
              <option>Social Media Marketing</option>
            </select>
          </div>

          <div class="form-group">
               
            <label for="exampleInputEmail1">Business Type </label>
            <select class="form-control" name="comodityOffered">
                <option>Products Goods </option>
                <option>Services</option>
              
              </select>
                           
        </div>

            <div class="form-group">
                
                <label for="exampleInputEmail1">Description of Product or Service Offered</label>
                <textarea class="form-control" rows="5" name="ProductDescription"></textarea>
                <label for="exampleInputEmail1">What is your Competitive Advantage</label>
                <select class="form-control" name="competitiveadvantage">
                    <option>Pattent / Trademark / CopyRight</option>
                    <option>Unique Formula / new Innovation</option>
                    <option>Strategic Location</option>
                    <option>Strategic connection or network</option>
                    <option>Talent / Experience</option>

                  </select>           
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"> where are You in the Supply Chain </label>
                <select class="form-control" name="supplyChainLevel">
                    <option>Manufacturer / Service Provider</option>
                    <option>Whole Seller Distributer</option>
                    <option>Broker / Intermediary</option>
                    <option>Retailler</option>
                  </select>
                          
            </div>
            <div class="form-group">
               
                <label for="exampleInputEmail1"> Product Re-Usability</label>
                <select class="form-control" name="productReusability">
                    <option>One Time Usage </option>
                    <option>Re-usable Product</option>
                  
                  </select>
                               
            </div>
            

 </div>
 <div class="col-md-5 ">
   
     
    <div class="form-group">
        <h3>FINANCIAL MANAGEMENT </h3>
      <label for="exampleInputEmail1">Amount applied for (Kshs)</label>
      <input type="number" required="" class="form-control" name="amount" placeholder="">
    </div>

    <div class="form-group">
            <label for="exampleInputEmail1">Investment Instrument</label>
            <select class="form-control" name="investmentInstrument">
               <option>Equity share</option>
               <option>Convertible Loan</option>
           
             </select>
         
           </div>
  
   <div class="form-group">
        
      <label for="exampleInputEmail1">Cost of project (Kshs)</label>
      <input type="number" required=""  class="form-control" name="projectCost" placeholder="">
    </div>
  
    
  
     <div class="form-group">
        
   
      <label for="exampleInputEmail1">Purpose</label>
      <textarea class="form-control" required="" rows="3" name="loanPurpose"></textarea>
  
    </div>
  
    <div class="form-group">
        
   
      <label for="exampleInputEmail1">Personal contribution (Kshs)</label>
      <input type="number" required=""  class="form-control" name="personalContribution" placeholder="">
  
    </div>
  

    <div class="form-group">
            <label for="exampleInputEmail1">Do you have any Existing Liability or Debt</label>
            <select class="form-control" name="liability">
               <option>Yes</option>
               <option>No</option>
           
             </select>
         
           </div>
    
           
    <div class="form-group">
            <label for="exampleInputEmail1">What is your Main Business Cost</label>
            <select class="form-control" name="mainbusinessCost">
               <option>Production Cost</option>
               <option>Marketing cost</option>
               <option>Transportation cost</option>
               <option>Rent cost</option>
               <option>Salaries & Wages cost</option>

             </select>
         
           </div>
        
        <div class="checkbox">
        </div>
                <button type="submit" class="btn btn-default">Submit</button>
        </div>


        <div class="col-md-5 ">
           


            <div class="form-group">
                <h3> SALES AND MARKETING</h3>
              <label for="exampleInputEmail1">Target Market Group</label>
              <select class="form-control" name="targetMarket">
                <option>Cooprates Clients</option>
                <option>Retail Clients</option>
                
              </select>
            </div>
            <div class="form-group">
                
                <label for="exampleInputEmail1">Income Group Of Client</label>
                <select class="form-control" name="incomeGroup">
                    <option>Low and Average Income customers</option>
                    <option>High Networth & income customers </option>
                    
                  </select>
              </div>
          
           <div class="form-group">
                
              <label for="exampleInputEmail1">Who are the Industry giants in your industry</label>
              <textarea class="form-control" rows="2" name="industryGiants"></textarea>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Product Delivery Mode</label>
                <select class="form-control" name="deliveryMode">
                   <option>Retail shop Location</option>
                   <option>online E-commerce</option>
                   <option>Physical delivery</option>
                   <option>Parcel Delivery</option>
                 </select>
             
               </div>
          
            <div class="form-group">
                
           
              <label for="exampleInputEmail1">What is your main mode of Marketing</label>
                  <select class="form-control" name="marketingMode">
                  <option>Digital Marketing : Radio / TV</option>
                  <option>Social Media Marketing</option>
                  <option>Evangelism / Word of mouth Marketing</option>
                  <option>Print media Marketing</option>
                  <option>Billboard Marketing</option>
                </select>
          
            </div>
          
                <div class="form-group">
                <label for="exampleInputEmail1">Product Resellability</label>
                <select class="form-control" name="resellability">
                    <option>Re-usable Product / Service : One time Sale</option>
                    <option>One time usage Product / Service : Repeat sale</option>
                
                </select>
            
                </div>
          
            <div class="form-group">
                <label for="exampleInputEmail1">How do you intend to grow Revenue</label>
                <select class="form-control" name="businessScalability">
                   <option>Increase volume of sales</option>
                   <option>High Magrins on each sale</option>
               
                 </select>
             
               </div>
        </div>
        
        {!! Form::close() !!}
        
</div>
</div>
</section>
@endsection































