<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/img-20180813-wa0009-122x122.jpg" type="image/x-icon">
  <meta name="description" content="Website Creator Description">
  <title>Investor</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">

  {{--  <!-- <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css"> -->  --}}
  
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
</head>
<body>
  {{--  <section class="menu cid-r6Wid0Ifmg" once="menu" id="menu2-h">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')

<section class="engine"><a href="https://mobirise.info/q">free responsive web templates</a></section><section class="header1 cid-r6WmJWrWPi mbr-parallax-background" id="header1-k">

    

    <div class="mbr-overlay" style="opacity: 0.2; background-color: rgb(0, 0, 0);">
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-1">Start investing in a start up today</h1>
                
                
                <div class="mbr-section-btn align-center">
                    <a class="btn btn-md btn-primary display-4" href="#features3-l">
                        <span class="mbr-iconfont mbri-file"></span>LEARN MORE</a>
                    <a class="btn btn-md btn-white-outline display-4" href="#">
                        <span class="mbr-iconfont mbri-idea"></span>GET STARTED</a>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="features3 cid-r6WnAftFQb" id="features3-l">

    

    
    <div class="container">
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/institutional-investor-360x262.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Institutional Investor</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Institutional investors are the biggest component of the so-called "smart money" group. There are generally six types of institutional investors: pension funds, endowment funds, insurance companies, commercial banks, mutual funds and hedge funds.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="https://mobirise.co" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/private-investor-676x409.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">Private investor</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Private investors are key for new businesses looking to raise start up capital. Not only do private investments bring financial help to the entrepreneur, but often these investors can provide expertise and contacts that the new business may need in order to get to the next level
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper">
                    <div class="card-img">
                        <img src="{{  asset('css/assets/images/crowdfunding-676x409.jpg')}}" alt="Mobirise" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">
                            CrowdFunding
                        </h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            Crowdfunding makes use of the easy accessibility of vast networks of people through social media and crowdfunding websites to bring investors and entrepreneurs together, and has the potential to increase entrepreneurship by expanding the pool of investors from whom funds can be raised beyond the traditional circle of owners, relatives and venture capitalists.
                        </p>
                    </div>
                    {{--  <div class="mbr-section-btn text-center">
                        <a href="#" class="btn btn-primary display-4">
                            Learn More
                        </a>
                    </div>  --}}
                </div>
            </div>

            
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r6Wqju0Dzy" id="content2-m">

     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7">
                <blockquote><p><strong>Every investor knows that the most scarse rescourse in bussiness are good ideas.&nbsp;</strong><span style="font-size: 1.09rem;">We at Startup  recorgnise that investors  are looking to find ventures that are innovative and  profitable that they can invest in. This is why we make the conscious effort of partnering  with entrepreneures who are eager to grow their bussiness and need you, the investor to walk  and grow with them.&nbsp;</span></p><p><span style="font-size: 1.09rem;">We recorgnise that there different industries that an investor could have interest in, from agriculture, technology, hotel and restaurants, construction, transport and many more, therefor  we have partnered with a diverse group of entrepreneures who  share the same interests with you and  will help you build profitable portfolio.&nbsp;</span></p><p>
<span style="font-size: 1.09rem;">Investors may organise themselves in different ways, whether you are an institutional, private or group investor,your interests will be channelled properly, through our  platform that helps you  connect with the right entrepreneure, so as to build a portfolio that is profitable.</span></p></blockquote>
            </div>
        </div>
    </div>
</section>



  <script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
  <script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
  <script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
  <script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
  <script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
  <script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
  <script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  
  
</body>
@endsection