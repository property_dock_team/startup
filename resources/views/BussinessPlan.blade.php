<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.8.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.8.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="{{  asset('css/assets/images/img-20180813-wa0009-122x122.jpg')}}" type="image/x-icon">
  <meta name="description" content="Web Site Maker Description">
  <title>Business Plan</title>
  <link rel="stylesheet" href="{{  asset('css/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/dropdown/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('css/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('css/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  
</head>
<body>
  {{--  <section class="menu cid-r6WJbhG2ku" once="menu" id="menu2-13">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://mobirise.co">
                        <img src="assets/images/logo2.png" alt="Mobirise" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Home</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Investor</a></li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Entrepreneur</a></li>
                <li class="nav-item">
                    <a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Crowd Funding</a>
                </li><li class="nav-item"><a class="nav-link link text-black display-4" href="https://mobirise.co">
                        Conacts</a></li></ul>
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="tel:+1-234-567-8901">
                    <span class="btn-icon mbri-mobile mbr-iconfont mbr-iconfont-btn">
                    </span>
                    +254715 576211</a></div>
        </div>
    </nav>
</section>  --}}
@extends('layouts.app')
@section('content')
<section class="engine"><a href="">responsive web templates</a></section><section class="mbr-section content5 cid-r70aiMPyVC mbr-parallax-background" id="content5-1g">

    

    

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    <br>Business Plans</h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">
                    How to write and structure your business plan</h3>
                
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r7062V2DAF" id="content1-1d">
    
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7"><p><strong>Business Plans</strong><br></p><p>For us to consider any invitation to invest, we need something that spells out clearly the business opportunity, who is going to deliver on the opportunity, and how we are invited to participate (including the expected returns from the venture).</p></div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r70674xIXG" id="content2-1e">

     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7">
                <blockquote><strong>We prefer business plans that are clear and simple. Every company should develop and use a succinct written business plan to guide its operations.</strong></blockquote>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-r70786CdZU" id="content1-1f">
    
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 col-md-8 mbr-fonts-style display-7"><strong>We expect a company's business plan to convey clearly the potential of the investment opportunity</strong>&nbsp;We do not expect a business plan to address every conceivable question. What's important to us is the quality of an idea, the nature of the opportunity, and the commitment management makes to achieving ambitious and attainable goals.
<div>
<span style="font-size: 1rem;">While business plans come in many varieties, and there are a number of acceptable ways to structure and present a business plan, we look for several key elements when we evaluate a business plan.</span></div></div>
        </div>
    </div>
</section>

<section class="mbr-section article content12 cid-r705F8qZ9A" id="content12-1c">
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text counter-container col-12 col-md-8 mbr-fonts-style display-7">
                <ul>
                    <li><strong>Executive Summary</strong>- &nbsp;What are your expectations of Startup as an investor</li>
                    <li><strong>Company Description</strong>&nbsp;- How it started, Where you are and where do you want to go and Who are the owners</li>
                    <li><strong>Product/ Service</strong>&nbsp;- What are you selling and How does your product differentiate itself&nbsp;</li><li><strong>Marketing Plan</strong> - Digital/Social media Marketing,Direct Marketing and Strategy and cost of Marketing</li><li><strong>Operations Plan</strong> - &nbsp;How are operations executed and What are the key variables.</li><li><strong>Financial Plan</strong> - Historical &amp; Projected Financials,P&amp;L, BS, Cash Flow statements and Financing Requirements</li><li><strong>Management Team</strong> &nbsp;- Who is the core management team, What is their experience and incentives</li>
                </ul>
            </div>
        </div>
    </div>
</section>



<script src="{{ asset('css/assets/web/assets/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('css/assets/popper/popper.min.js')}}"></script>
<script src="{{ asset('css/assets/tether/tether.min.js')}}"></script>
<script src="{{ asset('css/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('css/assets/smoothscroll/smooth-scroll.js')}}"></script>
<script src="{{ asset('css/assets/parallax/jarallax.min.js')}}"></script>
<script src="{{ asset('css/assets/dropdown/js/script.min.js')}}"></script>
<script src="{{ asset('css/assets/touchswipe/jquery.touch-swipe.min.js')}}"></script>
<script src="{{ asset('css/assets/vimeoplayer/jquery.mb.vimeo_player.js')}}"></script>
<script src="{{ asset('css/assets/theme/js/script.js')}}"></script>
  
  
</body>
@endsection